
# DotFiles management repository based on git+stow by @sincomil
## Requirements
You have to install [GNU Stow](https://www.gnu.org/software/stow/) for easy use
## Usage
1. Clone repository to your home directory
2. Make sure that you have no dotfiles/directory entries conflicting with this repository. Just run inside DotFiles direcotry:
```stow -vn */``` - this will show any conflicting entries.
3. Run stow for each Package(directory in DotFiles) you want to configure accordingly.



